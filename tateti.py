#!/usr/bin/python
# -*- coding: utf-8 -*-
from random import randint, choice


class Board:
    def __init__(self):
        self.board = [' '] * 10
        self.move = ' '
        self.player_letter = ''
        self.computer_letter = ''

    def view_board(self):
        print('   |   |')
        print(' ' + self.board[7] + ' | ' + self.board[8] + ' | ' + self.board[9])
        print('   |   |')
        print('-----------')
        print('   |   |')
        print(' ' + self.board[4] + ' | ' + self.board[5] + ' | ' + self.board[6])
        print('   |   |')
        print('-----------')
        print('   |   |')
        print(' ' + self.board[1] + ' | ' + self.board[2] + ' | ' + self.board[3])
        print('   |   |')

    def get_player_play(self):
        while self.move not in '1 2 3 4 5 6 7 8 9'.split() or not self.free_space(self.board, int(self.move)):
            print('¿What is your next move? (1-9)')
            self.move = input()
        return int(self.move)

    def full_board(self):
        for i in range(1, 10):
            if self.free_space(self.board, i):
                return False
        return True

    def get_play_computer(self, computer_letter):
        if computer_letter == 'X':
            self.player_letter = 'O'
        self.player_letter = 'X'

        for i in range(1, 10):
            copy_board = self.get_duplicate_board(self.board)
            if self.free_space(copy_board, i):
                self.do_play(copy_board, computer_letter, i)
                if self.winner(copy_board, computer_letter):
                    return i

        for i in range(1, 10):
            copy_board = self.get_duplicate_board(self.board)
            if self.free_space(copy_board, i):
                self.do_play(copy_board, player_letter, i)
                if self.free_space(copy_board, i):
                    return i
        self.move = self.choose_random_list([1, 3, 7, 9])
        if self.move is not None:
            return self.move
        if self.free_space(self.board, 5):
            return 5
        return self.choose_random_list([2, 4, 6, 8])

    def choose_random_list(self, play_list):
        possible_plays = [item for item in play_list if self.free_space(self.board, item)]
        if len(possible_plays) != 0:
            return choice(possible_plays)
        return None

    @staticmethod
    def do_play(board, player_letter, move):
        board[move] = player_letter

    @staticmethod
    def get_duplicate_board(board):
        dup_board = [item for item in board]
        return dup_board

    @staticmethod
    def free_space(board, move):
        return board[move] == ' '

    @staticmethod
    def new_game():
        print('¿You want to play again? (yes/not)?')
        return input().lower().startswith('y')

    @staticmethod
    def winner(board, letter):
        return ((board[7] == letter and board[8] == letter and board[9] == letter) or
                (board[4] == letter and board[5] == letter and board[6] == letter) or
                (board[1] == letter and board[2] == letter and board[3] == letter) or
                (board[7] == letter and board[4] == letter and board[1] == letter) or
                (board[8] == letter and board[5] == letter and board[2] == letter) or
                (board[9] == letter and board[6] == letter and board[3] == letter) or
                (board[7] == letter and board[5] == letter and board[3] == letter) or
                (board[9] == letter and board[5] == letter and board[1] == letter))


class Player:
    def __init__(self):
        self.letter = ''

    def enter_letter_player(self):
        while not (self.letter == 'X' or self.letter == 'O'):
            print('¿Do you want to be X or O?')
            self.letter = input().upper()
        if self.letter == 'X':
            return ['X', 'O']
        return ['O', 'X']

    @staticmethod
    def who_begins():
        if randint(0, 1) == 0:
            return 'Computer'
        return 'Player'


print('¡-----Welcome al Ta Te Ti!-----')

while True:
    board = Board()
    player = Player()
    player_letter, computer_letter = player.enter_letter_player()
    turn = player.who_begins()
    print("{} will go first.".format(turn))
    game = True

    while game:
        if turn == 'Player':
            board.view_board()
            move = board.get_player_play()
            board.do_play(board.board, player_letter, move)

            if board.winner(board.board, player_letter):
                board.view_board()
                print('¡Congratulations, you have won!')
                game = False
            if board.full_board():
                board.view_board()
                print("¡It's a tie!")
                break
            turn = 'Computer'

        move = board.get_play_computer(computer_letter)
        board.do_play(board.board, computer_letter, move)

        if board.winner(board.board, computer_letter):
            board.view_board()
            print('¡The computer has defeated you! Has lost!')
            game = False
        if board.full_board():
            board.view_board()
            print("¡It's a tie!")
            break
        turn = 'Player'

    if not board.new_game():
        break
